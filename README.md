# KKube

A short experiment in orchestrating Kubernetes using the [Fabric8 Java client](https://github.com/fabric8io/kubernetes-client)
and the [k8s-kotlin-dsl](https://github.com/fkorotkov/k8s-kotlin-dsl) project.

This app constructs a simple NGINX deployment and service that serves an HTML file. 
The project makes extensive use of Kotlin to introduce a consistent DSL.

## Running the app

```bash
./gradlew run 
```