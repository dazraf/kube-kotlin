package io.dazraf.k2

import com.fkorotkov.kubernetes.metadata
import com.fkorotkov.kubernetes.spec
import io.dazraf.k2.App.Companion.defaultLabels
import io.dazraf.k2.App.Companion.defaultSelector
import io.dazraf.k2.internal.extensions.ports
import io.fabric8.kubernetes.api.model.IntOrString
import io.fabric8.kubernetes.api.model.Service

open class MyService : Service() {
  init {
    metadata {
      name = "nginx-service"
      namespace = MyNamespace.NAMESPACE
      labels = defaultLabels
    }
    spec {
      type = "NodePort"
      selector = defaultSelector
      ports {
        port {
          protocol = "TCP"
          targetPort = IntOrString(80)
          port = 80
          nodePort = 30080
        }
      }
    }
  }
}