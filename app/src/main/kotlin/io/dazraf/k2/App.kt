package io.dazraf.k2

import io.dazraf.k2.internal.extensions.applyResources
import io.dazraf.k2.internal.extensions.kubeFlow
import io.dazraf.k2.internal.logging.appLogger

class App {
  companion object {
    const val APP_NAME = "nginx-app"
    val defaultLabels = mapOf("app" to APP_NAME)
    val defaultSelector = defaultLabels

    @JvmStatic
    fun main(args: Array<String>) {
      val contextName = args.getOrNull(0) ?: "docker-desktop"
      App().run(contextName)
    }
  }

  fun run(contextName: String ) {
    try {
      val resources = listOf(
        MyNamespace(),
        MyConfigMap(),
        MyDeployment(),
        MyService()
      )
      kubeFlow(contextName) { kube ->
        kube.applyResources(resources)
        // kube.applyResources(resources)
      }
    } catch (err: Throwable) {
      appLogger.error("execution failed", err)
    }
  }
}




