package io.dazraf.k2

import com.fkorotkov.kubernetes.apps.metadata
import com.fkorotkov.kubernetes.apps.selector
import com.fkorotkov.kubernetes.apps.spec
import com.fkorotkov.kubernetes.apps.template
import com.fkorotkov.kubernetes.configMap
import com.fkorotkov.kubernetes.metadata
import com.fkorotkov.kubernetes.spec
import io.dazraf.k2.App.Companion.APP_NAME
import io.dazraf.k2.App.Companion.defaultLabels
import io.dazraf.k2.App.Companion.defaultSelector
import io.dazraf.k2.MyNamespace.Companion.NAMESPACE
import io.dazraf.k2.internal.extensions.*
import io.fabric8.kubernetes.api.model.apps.Deployment

class MyDeployment : Deployment() {
  companion object {
    const val HTML_FILES_VOLUME = "html-files"
    const val NGINX_DEPLOYMENT = "nginx-deployment"
  }
  init {
    metadata {
      name = NGINX_DEPLOYMENT
      namespace = NAMESPACE
      labels = defaultLabels
    }
    spec {
      replicas = 1
      selector {
        matchLabels = defaultSelector
      }
      template {
        metadata {
          labels = defaultLabels
        }
        spec {
          containers {
            container {
              image = "nginx:latest"
              name = "nginx"
              ports {
                port {
                  containerPort = 80
                  name = "nginx"
                }
              }
              volumeMounts {
                volumeMount {
                  name = HTML_FILES_VOLUME
                  mountPath = "/usr/share/nginx/html/"
                  readOnly = true
                }
              }
            }
          }
          volumes {
            volume {
              name = HTML_FILES_VOLUME
              configMap {
                name = MyConfigMap.NAME
                items {
                  item {
                    key = "index.html"
                    path = "index.html"
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}