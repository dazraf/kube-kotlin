package io.dazraf.k2

import com.fkorotkov.kubernetes.metadata
import io.dazraf.k2.MyNamespace.Companion.NAMESPACE
import io.dazraf.k2.internal.files.resourceText
import io.fabric8.kubernetes.api.model.ConfigMap

class MyConfigMap: ConfigMap() {
  companion object {
    const val NAME = "html-files-config-map"
  }
  init {
    metadata {
      name = NAME
      namespace = NAMESPACE
    }
    data = mapOf(
      "index.html" to resourceText("index.html")
    )
  }
}