package io.dazraf.k2.internal.extensions

import io.dazraf.k2.internal.logging.appLogger
import io.fabric8.kubernetes.client.Config
import io.fabric8.kubernetes.client.DefaultKubernetesClient
import io.fabric8.kubernetes.client.KubernetesClient

fun kubeFlow(
  contextName: String,
  fn: (KubernetesClient) -> Unit
) {
  appLogger.info("connecting to kube with context $contextName")
  DefaultKubernetesClient(Config.autoConfigure(contextName)).use(fn)
}