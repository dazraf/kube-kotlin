package io.dazraf.k2.internal.logging

import io.fabric8.kubernetes.api.model.HasMetadata

fun HasMetadata.summarise(): String {
  val name = metadata?.name ?: "<unknown name>"
  return "$kind $name"
}