package io.dazraf.k2.internal.extensions

import io.dazraf.k2.internal.logging.appLogger
import io.dazraf.k2.internal.logging.summarise
import io.fabric8.kubernetes.api.model.*
import io.fabric8.kubernetes.api.model.apps.Deployment
import io.fabric8.kubernetes.client.KubernetesClient

fun KubernetesClient.applyResources(vararg resources: HasMetadata) {
  applyResources(resources.toList())
}

fun KubernetesClient.applyResources(resources: List<HasMetadata>) {
  removeDeadResources(resources)
  resources.filterIsInstance<Namespace>().forEach { applyResource(it) }
  resources.filterIsInstance<ConfigMap>().forEach { applyResource(it) }
  resources.filterIsInstance<Deployment>().forEach { applyResource(it) }
  resources.filterIsInstance<Service>().forEach { applyResource(it) }
}

fun KubernetesClient.removeResources(vararg resources: HasMetadata) {
  removeResources(resources.toList())
}

fun KubernetesClient.removeResources(resources: List<HasMetadata>) {
  removeDeadResources(resources)
  resources.filterIsInstance<Service>().forEach { removeResource(it) }
  resources.filterIsInstance<Deployment>().forEach { removeResource(it) }
  resources.filterIsInstance<ConfigMap>().forEach { removeResource(it) }
  resources.filterIsInstance<Namespace>().forEach { removeResource(it) }
}

// -- PRIVATE --

private fun KubernetesClient.removeDeadResources(resources: List<HasMetadata>) {
  val kindNameResource =
    resources.groupBy { it.kind }.mapValues { (_, resourcesByKind) -> resourcesByKind.map { it.metadata.name }.toSet() }
  kindNameResource.forEach { (kind, names) ->
    when (kind) {
      ConfigMap::class.simpleName -> removeDeadConfigMaps(names)
      Service::class.simpleName -> removeDeadServices(names)
      Deployment::class.simpleName -> removeDeadDeployments(names)
    }
  }
}

private fun KubernetesClient.removeDeadConfigMaps(names: Set<String>) {
  val configmaps = configMaps().inNamespace(this.namespace)
  val toDelete = configmaps.list().items.filter { !names.contains(it.metadata.name) }
  configmaps.delete(toDelete)
}

private fun KubernetesClient.removeDeadServices(names: Set<String>) {
  val services = services().inNamespace(this.namespace)
  val toDelete = services.list().items.filter { !names.contains(it.metadata.name) }
  services.delete(toDelete)
}

private fun KubernetesClient.removeDeadDeployments(names: Set<String>) {
  val deployments = apps().deployments().inNamespace(this.namespace)
  val toDelete = deployments.list().items.filter { !names.contains(it.metadata.name) }
  deployments.delete(toDelete)
}

private fun KubernetesClient.applyResource(namespace: Namespace): Namespace {
  appLogger.info("creating or replacing ${namespace.summarise()}")
  return namespaces().createOrReplace(namespace)
}

private fun KubernetesClient.removeResource(namespace: Namespace) {
  appLogger.info("creating or replacing ${namespace.summarise()}")
  namespaces().delete(namespace)
}

private fun KubernetesClient.applyResource(configMap: ConfigMap): ConfigMap {
  appLogger.info("creating or replacing ${configMap.summarise()}")
  return configMaps().inNamespace(configMap.metadata.namespace).createOrReplace(configMap)
}

private fun KubernetesClient.removeResource(configMap: ConfigMap) {
  appLogger.info("removing ${configMap.summarise()}")
  configMaps().inNamespace(configMap.metadata.namespace).delete(configMap)
}

private fun KubernetesClient.applyResource(deployment: Deployment): Deployment {
  appLogger.info("creating or replacing ${deployment.summarise()}")
  return apps().deployments().inNamespace(deployment.metadata.namespace).createOrReplace(deployment)
}

private fun KubernetesClient.removeResource(deployment: Deployment) {
  appLogger.info("removing ${deployment.summarise()}")
  apps().deployments().inNamespace(deployment.metadata.namespace).delete(deployment)
}

private fun KubernetesClient.applyResource(service: Service) {
  appLogger.info("creating or replacing ${service.summarise()}")
  // we have to remove the service explicitly because
  // using the same node port causes an exception in kube.
  services().inNamespace(service.metadata.namespace).delete(service)
  services().inNamespace(service.metadata.namespace).createOrReplace(service)
}

private fun KubernetesClient.removeResource(service: Service) {
  appLogger.info("removing ${service.summarise()}")
  // we have to remove the service explicitly because
  // using the same node port causes an exception in kube.
  services().inNamespace(service.metadata.namespace).delete(service)
}
