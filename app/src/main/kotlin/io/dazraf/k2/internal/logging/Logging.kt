package io.dazraf.k2.internal.logging

import org.slf4j.Logger
import org.slf4j.LoggerFactory

val appLogger: Logger = LoggerFactory.getLogger("k2")
