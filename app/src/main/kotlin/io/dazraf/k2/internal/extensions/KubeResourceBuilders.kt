package io.dazraf.k2.internal.extensions

import com.fkorotkov.kubernetes.*
import io.fabric8.kubernetes.api.model.*


fun ServiceSpec.ports(fn: ServiceSpecPortsBuilder.() -> Unit) {
  ServiceSpecPortsBuilder(this).apply(fn).build()
}

data class ServiceSpecPortsBuilder(private val serviceSpec: ServiceSpec) {
  private val ports = mutableListOf<ServicePort>()
  fun port(fn: ServicePort.() -> Unit) {
    ports.add(newServicePort(fn))
  }

  internal fun build() {
    serviceSpec.ports = ports
  }
}

fun Container.ports(fn: ContainerPortsBuilder.() -> Unit) {
  ContainerPortsBuilder(this).apply(fn).build()
}

data class ContainerPortsBuilder(private val container: Container) {
  private val ports = mutableListOf<ContainerPort>()
  fun port(fn: ContainerPort.() -> Unit) {
    ports.add(newContainerPort(fn))
  }

  internal fun build() {
    container.ports = ports
  }
}

fun PodSpec.volumes(fn: PodSpecVolumeBuilder.() -> Unit) {
  PodSpecVolumeBuilder(this).apply(fn).build()
}

data class PodSpecVolumeBuilder(private val podSpec: PodSpec) {
  private val volumes = mutableListOf<Volume>()

  fun volume(fn: Volume.() -> Unit) {
    volumes.add(newVolume(fn))
  }

  internal fun build() {
    podSpec.volumes = volumes
  }
}

fun Container.volumeMounts(fn: ContainerVolumeMountsBuilder.() -> Unit) {
  ContainerVolumeMountsBuilder(this).apply(fn).build()
}

data class ContainerVolumeMountsBuilder(private val container: Container) {
  private val volumeMounts = mutableListOf<VolumeMount>()
  fun volumeMount(fn: VolumeMount.() -> Unit) {
    volumeMounts.add(newVolumeMount(fn))
  }
  internal fun build() {
    container.volumeMounts = volumeMounts
  }
}

fun PodSpec.containers(fn: PodSpecContainersBuilder.() -> Unit) {
  PodSpecContainersBuilder(this).apply(fn).build()
}

data class PodSpecContainersBuilder(private val podSpec: PodSpec) {
  private val containers = mutableListOf<Container>()

  fun container(fn: Container.() -> Unit) {
    containers.add(newContainer(fn))
  }

  internal fun build() {
    podSpec.containers = containers
  }
}

fun ConfigMapVolumeSource.items(fn: ConfigMapVolumeSourceItemsBuilder.() -> Unit) {
  ConfigMapVolumeSourceItemsBuilder(this).apply(fn).build()
}

data class ConfigMapVolumeSourceItemsBuilder(private val configMapVolumeSource: ConfigMapVolumeSource) {
  private val items = mutableListOf<KeyToPath>()
  fun item(fn: KeyToPath.() -> Unit) {
    items.add(newKeyToPath(fn))
  }
  internal fun build() {
    configMapVolumeSource.items = items
  }
}

