package io.dazraf.k2.internal.files

fun resourceText(path: String) = ClassLoader.getSystemResource(path).readText()