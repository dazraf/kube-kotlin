package io.dazraf.k2

import com.fkorotkov.kubernetes.metadata
import io.fabric8.kubernetes.api.model.Namespace

class MyNamespace: Namespace() {
  companion object {
    const val NAMESPACE = "nginx-namespace"
  }
  init {
    metadata {
      name = NAMESPACE
    }
  }
}